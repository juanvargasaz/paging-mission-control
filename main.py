from collections import defaultdict
import datetime
import json

INPUT_FILENAME = "test.txt"
TARGET_MATCHED_ITEMS = 3
MAX_INTERVAL_IN_MINUTES = 5

# Ideally this would be an enum but I am too lazy for that.
COMPONENT_SEVERITY_MAP = {
  "TSTAT": "RED HIGH",
  "BATT": "RED LOW"
}

def main():
    input_file = open(INPUT_FILENAME, "r")

    # Create a dictionary for satellites that meet the criteria for warnings with the key being an int - the satellite number
    # and the value being a dict - with keys being a string for the type (battery vs thermostat) and the value being an array with the values. 
    satellites_with_warning_dict = defaultdict(lambda: defaultdict(list))
    populate_satellites_with_warning_dict(input_file, satellites_with_warning_dict)

    input_file.close()
    
    result = []
    populate_result_array(result, satellites_with_warning_dict)

    print(json.dumps(result, indent=4))
   
def populate_satellites_with_warning_dict(input_file, satellites_dict):
    for line in input_file:
        info_array = line.split("|")
        timestamp = info_array[0]
        satellite_number = int(info_array[1])
        component = info_array[-1].strip()
        value = float(info_array[-2])
        red_high_limit = int(info_array[2])
        red_low_limit = int(info_array[-3])

        # We should probably use the enum I didn't create here instead of using hard coded values.
        if ((component == "TSTAT" and value > red_high_limit)
            or (component == "BATT" and value < red_low_limit)):
            satellites_dict[satellite_number][component].append(timestamp)

# Use a two pointer algorith to find if TARGET_MATCHED_ITEMS timestamps are within MAX_INTERVAL_IN_MINUTES time.
def populate_result_array(result, satellites_dict):
    for satellite_number, components_dict in satellites_dict.items():
        for component, timestamps_array in components_dict.items():
            array_length = len(timestamps_array)
            beginning_index = 0
            end_index = 2
            while (end_index < array_length):
                time1 = datetime.datetime.strptime(timestamps_array[beginning_index], "%Y%m%d %H:%M:%S.%f")
                time2 = datetime.datetime.strptime(timestamps_array[end_index], "%Y%m%d %H:%M:%S.%f")
                time_difference = time2 - time1

                if (time_difference.total_seconds() / 60 < MAX_INTERVAL_IN_MINUTES):
                    # Append the match data as json.
                    data = {
                        "satelliteId": satellite_number,
                        "severity": COMPONENT_SEVERITY_MAP[component],
                        "component": component,
                        "timestamp": time1.isoformat()[:-3] + "Z"
                    }

                    result.append(data)

                beginning_index += 1
                end_index += 1

if __name__=="__main__": 
    main() 